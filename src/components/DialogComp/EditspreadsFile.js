import React, { useState, useEffect } from 'react';
import { restRequest } from '../../serverAPI/restRequests';
import Button from '@material-ui/core/Button';
import { DialogTitle, CircularProgress } from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextFieldComp from '../TextFieldComp/TextFieldComp';
import InfoDialogComp from './InfoDialogComp';
import ComboBoxComp from '../ComboBoxComp/ComboBoxComp';
import comboStyle from '../ComboBoxComp/ComboBoxComp.module.css';
import DateComp from '../DateComp/DateComp';
import { Alert } from '@material-ui/lab';

export default function EditSpreadsFile({ open, handleClose, prevDate, prevName, prevUserName, userList }) {
    console.log(prevDate, prevName, prevUserName);
    const [userName, setUserName] = useState(prevUserName);
    const [date, setDate] = useState(new Date(prevDate));
    const [areaName, setAreaName] = useState(prevName);
    const [doneChangesConfirmation, setDoneChangesConfirmation] = useState(false);
    const [loading, setLoading] = useState(false);

    function handleCloseDialog() {
        handleClose();
    }

    async function handleSaveDialog() {
        let formattedDate = date.toLocaleDateString('en-US', {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric'
        });

    }

    return (
        <div>
            <Dialog open={open} onClose={handleCloseDialog}>
                <DialogTitle>Edit Area</DialogTitle>
                <DialogContent>
                    <TextFieldComp value={areaName} setValue={setAreaName} label={"Area name"} />
                    <DateComp style="display: grid;" name={"Spread Date"} value={date} setValue={setDate} />
                    <ComboBoxComp name={"User:"} paramsList={userList} value={userName} setValue={setUserName} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleSaveDialog} color="primary" disabled={date === null || areaName === null || areaName === '' || userName === null} >
                        Save
                    </Button>
                    {loading &&
                        <CircularProgress size={30} disableShrink />
                    }
                </DialogActions>
            </Dialog>

            <InfoDialogComp message={'changes made successfully'} open={doneChangesConfirmation} handleClose={handleCloseDialog} />
        </div >
    );
}