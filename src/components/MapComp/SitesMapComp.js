import React, { useEffect, useState } from "react";
import { restRequest } from '../../serverAPI/restRequests';
import { GoogleMap, Marker, useLoadScript } from "@react-google-maps/api";
import Search from './Components/Search';
import Locate from './Components/Locate';
import SiteNamePopUpComp from '../PopUpComp/SiteNamePopUpComp';
import mapStyles from './CollectionsMapComp.module.css';
import { CircularProgress } from "@material-ui/core";

const libraries = ["places"];
const mapContainerStyle = {
    width: "99%",
    marginTop: "1.15%",
    height: "100%",
};
const center = {
    lat: 38.885086,
    lng: -121.969131,
};
const options = {
    disableDefaultUI: false,
    zoomControl: true,
};

export default function MapComp({ setLocation, textLong, textLat, createNew }) {
    const { isLoaded, loadError } = useLoadScript({
        googleMapsApiKey: "AIzaSyBYcFcT4RNo0gFqnYVBRRC8kGBJ2S1SKGI",
        libraries,
    });

    // storing the selected element on the map the user clicked on
    const [selectedSite, setSelectedSite] = useState(null);

    // the sites showing on the map 
    const [markers, setMarkers] = useState(null);
    const [companySites, setCompanySites] = useState(null);
    const [numSites, setNumSites] = useState(0);

    // for avoiding re-renders we pass the mapRef
    const mapRef = React.useRef();

    // for avoiding double marker set
    var map_click_flag = false;

    async function getCompanyLocation() {
        const response = await restRequest(`getCompanyLocation`, 'get', {});
        mapRef.current.panTo({ lat: response.data.lat, lng: response.data.lng });
    }

    const onMapLoad = React.useCallback((map) => {
        mapRef.current = map;
        getCompanyLocation();
    }, []);

    function addMarker(tmp_markers, lat, long) {
        tmp_markers.set(
            '1', {
            latitude: lat,
            longitude: long,
            name: '',
        });
        setMarkers(tmp_markers);
    }

    const onMapClick = React.useCallback((event) => {
        if (createNew) {
            map_click_flag = true;
            if (markers !== null) {
                let tmp_markers = new Map(markers);
                if (markers.size !== numSites) {
                    tmp_markers.delete('1');
                }
                addMarker(tmp_markers, event.latLng.lat(), event.latLng.lng());
                setLocation({ long: event.latLng.lng().toString(), lat: event.latLng.lat().toString() })
            }
        }
    });

    // pin down when searching address
    const panTo = React.useCallback(({ lat, lng }) => {
        mapRef.current.panTo({ lat, lng });
        mapRef.current.setZoom(14);
    }, []);

    async function getCompanySites() {
        const response = await restRequest('getCompanySites', 'get', {});
        const allSites = response.data;
        var allSitesByIds = new Map();
        for (let i = 0; i < allSites.length; i++)
            allSitesByIds = allSitesByIds.set(allSites[i]._id, { longitude: allSites[i].longitude, latitude: allSites[i].latitude, name: allSites[i].name });
        setMarkers(allSitesByIds);
        setNumSites(allSitesByIds.size);
        setCompanySites(allSitesByIds);
    }

    useEffect(() => {
        getCompanySites();
    }, [])

    useEffect(() => {
        if (markers !== null && !map_click_flag) {
            let tmp_markers = new Map(markers);
            if (markers.size > numSites)
                tmp_markers.delete('1');
            addMarker(tmp_markers, parseFloat(textLat), parseFloat(textLong));
        }
        map_click_flag = false;
    }, [textLong, textLat])

    if (loadError) return "Error loading maps";
    if (!isLoaded) return "Loading maps";

    const get_icon = (marker) => {
        if (marker.name === "")
            return `/deployed_selected.svg`;
        else
            return `/deployed.svg`;
    }

    return (
        <div style={{widht: "100%", height: "100%"}}>
        {markers !== null && 
            <GoogleMap mapContainerStyle={mapContainerStyle} zoom={9} center={center} options={options} onClick={onMapClick} onLoad={onMapLoad}>
                {// gets iterator return array
                Array.from(markers.values()).map((marker) => (
                    <Marker
                    position={{ lat: marker.latitude, lng: marker.longitude }}
                    onClick={() => {
                        setSelectedSite(marker);
                    }}
                    icon={{
                        url: get_icon(marker),
                        origin: new window.google.maps.Point(0, 0),
                        anchor: new window.google.maps.Point(15, 15),
                        scaledSize: new window.google.maps.Size(30, 30),
                    }}
                    />            
                ))}
                {selectedSite && selectedSite.name !== "" &&
                    <SiteNamePopUpComp selectedSite={selectedSite} setSelectedSite={setSelectedSite}/>
                }
                <Search className={mapStyles.searchSite} panTo={panTo} />
                <Locate className={mapStyles.locate} panTo={panTo} />
            </GoogleMap>
        }
        {markers === null &&
            <CircularProgress style={{position : 'absolute', top : "50%", left : "60%"}} size={60} disableShrink />
        }
        </div>
    );
}