import React from 'react';
import { InfoWindow } from "@react-google-maps/api";
import classes from './SitePopUpComp.module.css';

export default function SitePopUpComp({ selectedSite, setSelectedSite }) {

    return (
        <InfoWindow position={{lat: selectedSite.latitude, lng: selectedSite.longitude}} onCloseClick={() => { setSelectedSite(null); }}>
            <div>
                <h1 className={classes.siteInfo}>{selectedSite.name}</h1>
            </div>
        </InfoWindow> 
    )
}