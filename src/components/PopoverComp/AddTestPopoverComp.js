import React, { useEffect, useState } from 'react';
import { restRequest } from '../../serverAPI/restRequests';
import { makeStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import ButtonComp from '../ButtonComp/ButtonComp';
import ComboBoxComp from '../ComboBoxComp/ComboBoxComp';
import buttonStyles from '../ButtonComp/ButtonComp.module.css';
import comboStyles from '../ComboBoxComp/ComboBoxComp.module.css';
import TextFieldComp from '../TextFieldComp/TextFieldComp';

const useStyles = makeStyles((theme) => ({
    typography: {
        padding: theme.spacing(2),
    },
}));

export default function AddTestPopoverComp({ checkedPooling, pooling, setAddedTest }) {

    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);

    const [virusTested, setVirusTested] = useState(null);
    const [testsList, setTestsList] = useState([]);
    const [result, setResult] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    function getPoolingObj() {
        let checked_pooling_obj;
        for (let i = 0; i < pooling.length; i++) {
            if (pooling[i]._id === checkedPooling)
                checked_pooling_obj = pooling[i]
        }
        return checked_pooling_obj
    }

    async function getTestsList() {
        const checked_pooling_obj = getPoolingObj();
        const response = await restRequest('getTests', 'post', { pool_obj: checked_pooling_obj });
        setTestsList(response.data);
    }

    async function addTest() {
        const checked_pooling_obj = getPoolingObj();
        await restRequest('addTest', 'post', { pool_obj: checked_pooling_obj, test: test, result: result === "Positive" });
        setAddedTest(true);
    }

    useEffect(() => {
        if (checkedPooling !== [])
            getTestsList();
    }, []);

    return (
        <div>
            <div>
                <ButtonComp className={buttonStyles.addTest} name={"Add test"} action={handleClick} />
            </div>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'center',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'center',
                    horizontal: 'center',
                }}
            >
                <Typography className={classes.typography}>
                    <TextFieldComp className={classes.vialInfo} value={virusTested} setValue={setVirusTested} label={"virus tested"} required={true} />
                    <ComboBoxComp className={comboStyles.addResult} name={"Result:"} paramsList={["Positive", "Negative"]} value={result} setValue={setResult} />
                    <ButtonComp className={buttonStyles.createTest} name={"Create"} action={addTest} isDisabled={result === null || virusTested === null} />
                </Typography>
            </Popover>
        </div>
    );
}