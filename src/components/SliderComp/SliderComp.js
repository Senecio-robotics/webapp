import * as React from 'react';
import Slider from '@mui/material/Slider';
import classes from './SliderComp.module.css';
import Box from '@mui/material/Box';

export default function DiscreteSlider({days,setDays}) {
    return (
        <Box className={classes.slider}>
            <la className={classes.label}>Days</la>
            <Slider
                aria-label="Days"
                defaultValue={7}
                getAriaValueText={days=>'-$days'}
                valueLabelDisplay="auto"
                step={7}
                min={-100}
                max={0}
            />
        </Box>
    )
}